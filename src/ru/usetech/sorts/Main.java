package ru.usetech.sorts;

import java.io.File;
import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {

//        задание пути для работы с файлом
        String textFilePath = "C://Users//user//Desktop//repos//MyTextFile.txt";
//        задание количества элементов в текстовом файле
        int lengthArr = 30;
//        задание верхней границы максимально возможных чисел, автоматически сгененрированных в файде [0; maxPossibbleElement]
        int maxPossibbleElement = 20;
//        задание разделителя между элементами в текстовом файле
        String delimeterInTextFile = ",";

//        создадим новый файл
        File newFile = new File(textFilePath);
        try {
            boolean created = newFile.createNewFile();
            if (created)
                System.out.println("File has been created");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

//        создадим массив из чисел из промежутка[0; maxPossibbleElement] размерностью в lengthArr элемент(количество элементов заранее не было указано в задании)
        int[] array = new int[lengthArr-1];
        final Random random = new Random();
        for (int i = 0; i < array.length; ++i)
            array[i] = random.nextInt(maxPossibbleElement+1);

//        запишем созданный массив в файл
        try (final FileWriter writer = new FileWriter(textFilePath, false)) {
            for (int i = 0; i < array.length; ++i) {
                final String s = Integer.toString(array[i]);
                writer.write(s);

                if (i < (array.length - 1)) {
                    writer.write(delimeterInTextFile);
                }
            }
            System.out.println("Data from text file : " + Arrays.toString(array));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

//        прочитаем числа из файла и запишем их в массив
        int[] newArr = new int[lengthArr];
        int i = 0;
        try (Scanner scanner = new Scanner(new File(textFilePath)).useDelimiter("\\s*" + delimeterInTextFile + "\\s*")) {
            while (scanner.hasNext()) {
                if (scanner.hasNextInt()) {
                    newArr[i++] = scanner.nextInt();
                }
            }
        } catch (InputMismatchException ex) {
            System.out.println(ex.getMessage());
        }

//        отсортируем массив по возрастанию и выведем результат в консоль
        Arrays.sort(newArr);
        System.out.println("Data from text file in ascending order : " + Arrays.toString(newArr));

//        отсортируем массив по убыванию и выведем результат в консоль
        Integer[] newArrBoxed = Arrays.stream(newArr).boxed().toArray(Integer[]::new);
        Arrays.sort(newArrBoxed, Collections.reverseOrder());
        System.out.println("Data from text file in descending order : " + Arrays.toString(newArrBoxed));
    }
}
